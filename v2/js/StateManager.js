var StateManager = (function () {

	'use strict';
	var cmd = "CURRENTSTATE";
	var state = null;

	function matchesCommand (msg) {
		console.log("Checking " + msg.cmd + " vs " + cmd);
		return msg.cmd.includes(cmd);
	}	

	function run (msg) {
		console.log ("Running " + state);
		console.log (msg);
		if (msg.action === "SET") {
			console.log("Setting my state to: [" + msg.payload.state + "]");
			setState(msg.payload.state);
		} 
	}

	function setState (s) {
		state = s;
	}

	function getState () {
		return state;
	}

	return {
		matchesCommand		: matchesCommand,
		run								:	run,
		setState          : setState,
		getState          : getState
	}
}());

