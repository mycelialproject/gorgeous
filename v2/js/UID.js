var UID = (function () {

	'use strict';
	var self = {};

	// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
	function uuidv4() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
			});
	}

	function getFingerprint () {
		new Fingerprint2({excludeAudio: true})
			.get(function(hash, components) {
				self.uid = result;
				self.fingerprint_components = components;
			});
	}

	function init () {
		if (Cookies.get("fingerprint")) {
			self.uid = Cookies.get("fingerprint");
		} else {
			getFingerprint();
			if (!self.uid) {
				self.uid = uuidv4();
			}
			// Either way, set the cookie.
			Cookies.set("fingerprint", self.uid);
		}

	}

	function uid () {
		return self.uid;
	}

	return {
		init : init,
		uid  : uid
	}

}());

