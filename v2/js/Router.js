var Router = (function () {

	'use strict';
	var channels  = ["commands_to_clients", "requests_from_clients"];
	var client    = null;
	var self      = {};

	// Cached DOM elements
	var DOM = {};

	function cacheDOM () {

	}

	function bindEvents () {

	}
	
	function subscribeToChannels () {
		for (var ndx in channels) {
			console.log("\t ... subscribing to " + channels[ndx]);
			client.subscribe(channels[ndx]);
		}
	}

	
	function onConnect () {
		console.log("Router connected.");
		subscribeToChannels();
		self.connected = true;
	}

	function onConnectionLost(responseObject) {
		if (responseObject.errorCode !== 0) {
			console.log("onConnectionLost:"+responseObject.errorMessage);
		}
	}	

	function onMessageReceived (msg) {
		console.log("RECEIVED");
		console.log(msg);
		var obj    = JSON.parse(msg.payloadString);
		// Add in the destination
		// In Python, this is the topic. In JS, it is the 
		// destinationName. Unifying here...
		obj.topic = msg.destinationName;

		Handlers.process(obj);
	}

	function initMQTT () {
		console.log("Entering initMQTT");
		client = new Paho.MQTT.Client(self.host, Number(self.port), self.uid);
		client.onMessageArrived = onMessageReceived;
		client.onConnectionLost = onConnectionLost;

		client.connect({onSuccess : onConnect});
	}

	function requestCurrentState () {
		console.log("Requesting current state.");
		var obj  =	{	action : "GET",
									cmd    : "CURRENTSTATE",
									uid    : UID.uid()
								};

		var body = JSON.stringify(obj);
		var message = new Paho.MQTT.Message(body);
		message.destinationName = "requests_from_clients";
		client.send(message);
	}

	function init (host, port, uid) {
		self.host      = host;
		self.port      = port;
		self.uid       = uid;
		self.connected = false;

		cacheDOM();
		bindEvents();
	
		return new Promise ((resolve, reject) => {
			initMQTT();
		});

	}

	return {
		init                : init,
		requestCurrentState : requestCurrentState
	}

}());

