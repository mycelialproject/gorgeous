var Handlers = (function () {

	'use strict';
	var handlers = [];

	function process (msg) {
		// Check if the message is for us.
		if (msg.topic === "commands_to_clients") {
			if (msg.uid.includes("ALL") || msg.uid.includes(UID.uid())) {
				console.log("Received a message for me.");
				for (var ndx in handlers) {
					if (handlers[ndx].matchesCommand(msg)) {
						handlers[ndx].run(msg);
						// Do not run multiple handlers once one matches.
						break;
					}
				} // loop over handlers
			} // is for us?
		} // destinationName
	}
	
	function init () {
		console.log("Handlers.init() called.");
		// Push the handlers onto the array.
		handlers.push(StateManager);
	}

	return {
		init			: init,
		process		: process
	};

}());
